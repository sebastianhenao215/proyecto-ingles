document.addEventListener('DOMContentLoaded', function () {
    // Define las respuestas correctas
    const respuestasCorrectas = {
        pregunta1: 'apple',
        // Agrega más preguntas y respuestas aquí
        pregunta2: 'orange'
    };

    const preguntas = document.querySelectorAll('.pregunta');
    const verificarBtn = document.getElementById('verificar-btn');
    const puntuacionSpan = document.getElementById('puntos');

    verificarBtn.addEventListener('click', function () {
        let puntuacion = 0;

        preguntas.forEach(pregunta => {
            const idPregunta = pregunta.id;
            const inputRespuesta = document.getElementById(`respuesta${idPregunta.slice(-1)}`);
            const respuestaUsuario = inputRespuesta.value.toLowerCase();
        
            if (respuestaUsuario === respuestasCorrectas[idPregunta]) {
                puntuacion++;
                pregunta.classList.add('correcta');
                pregunta.classList.remove('incorrecta'); // Elimina la clase incorrecta si estaba presente
            } else {
                pregunta.classList.remove('correcta'); // Elimina la clase correcta si estaba presente
                pregunta.classList.add('incorrecta');
            }
        });

        // Muestra la puntuación
        puntuacionSpan.textContent = puntuacion;
    });
});

